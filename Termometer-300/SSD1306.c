/* ----------------------------------------------------------------------------
	128x64 Graphic LCD management for SSD1306 driver

	FILE NAME 	: SSD1306.c
	LAYER		: Application
	
	DESCRIPTION	: The purpose of this function is to manage a graphic LCD
			  by providing function for control and display text and 
			  graphic 
			  
	AUTHOR		: Gabriel Anzziani
    www.gabotronics.com

 */

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>

#include "SoftI2CMaster.h"
#include "Fonts.h"
#include "SSD1306.h"

#define delay_us(x)		_delay_us(x)

#define STORE_DISPLAY_INIT_TABLE_IN_PROGMEM		0	
#define DISPLAY_ORIENTATION						1

#define setbit(x, y)	x |= (1 << y)
#define clrbit(x, y)	x &= ~(1 << y)

#ifdef USE_DOUBLE_BUFFER
uint8_t     display_buffer[1024];
#endif

uint8_t     LCD_Negative	= 0;
uint8_t		char2charInterval = 1;

uint8_t		isDisplayON		= 0;

uint8_t     dispx=0;
uint8_t     dispPage=0;    

/******************************************************************************
/                       DECLARATIONS / DEFINITIONS                            /
/ ****************************************************************************/

/* Local functions prototypes */
void LcdInstructionWrite (unsigned char);
void LcdWaitBusy (void);

/* General use definitions */
#define BUSY		0x80

/* INIT */
const uint8_t lcd_init[] 
#if STORE_DISPLAY_INIT_TABLE_IN_PROGMEM == 1
	PROGMEM 
#else
	EEMEM
#endif
	= {
	LCD_DISP_OFF, 
	LCD_SET_RATIO_OSC,
	0x80,
	LCD_MULTIPLEX,
	0x3F,
	LCD_SET_OFFSET,
	0x00,
	LCD_SET_LINE,
	LCD_CHARGE_PUMP,
	LCD_PUMP_OFF,
#if DISPLAY_ORIENTATION == 1
	LCD_SET_SEG_REMAP1,
	LCD_SET_SCAN_NOR,
#else
	LCD_SET_SEG_REMAP0,
	LCD_SET_SCAN_FLIP,
#endif
	LCD_SET_PADS,
	0x12,
	LCD_SET_CONTRAST,
	0xFF,
	LCD_SET_CHARGE,
	0xF1,
	LCD_SET_VCOM,
	0x40,
	LCD_EON_OFF,
	LCD_DISP_NOR,
	LCD_MEM_ADDRESSING,
	0x00          // Horizontal Addressing mode
	};

/******************************************************************************
/                       DECLARATIONS / DEFINITIONS                            /
/ ****************************************************************************/

struct FONT_DEF *font;

/******************************************************************************
/                               PROGRAM CODE                                  /
/ ****************************************************************************/

uint8_t isDisplayOn()
{
	return isDisplayON;
}

void LCD_start_Data_transfer(void)
{
	i2c_start(0b01111000);
	i2c_write(0x40);
}

void GLCD_soft_on(char on)
{
	i2c_start(0b01111000);
	LcdInstructionWrite(on ? LCD_DISP_ON : LCD_DISP_OFF);
	i2c_stop();
}

uint8_t GLCD_on(char on)
{
	if (on != isDisplayON)
	{
		if (on)
		{
			// ����, ���������
			GLCD_LcdInit();
			GLCD_soft_on(1);
		}
		else
		{
			GLCD_soft_on(0);
			// �����
			//setbit(LCD_DDR, LCD_RES);
			//clrbit(LCD_PORT, LCD_RES);
		}
	
		isDisplayON = on;
		return 1;
	}
	return 0;
}

/*-----------------------------------------------------------------------------
LCD Initialization
	GLCD_LcdINIT()
-----------------------------------------------------------------------------*/
void GLCD_LcdInit(void)	{
    // Recommended power up sequence
	int i;
	setbit(LCD_DDR, LCD_RES);
	
	setbit(LCD_PORT, LCD_RES);	
  	delay_us(1);
    clrbit(LCD_PORT, LCD_RES);         // Reset Low for 3 uS
    delay_us(10);
	setbit(LCD_PORT, LCD_RES);
	delay_us(10);
	
    i2c_start(0b01111000);
	
    // Recommended intialization sequence
	uint8_t* pointer = lcd_init;
	
	for (i = 0; i < sizeof(lcd_init); ++i)
		LcdInstructionWrite(
#if STORE_DISPLAY_INIT_TABLE_IN_PROGMEM == 1
			pgm_read_byte(pointer++)
#else
			eeprom_read_byte(pointer++)
#endif
			);
			
	i2c_stop();
    
    font = default_Font;
}

/*-------------------------------------------------------------------------------
Send an image to the LCD
	GLCD_DisplayPicture (uint8_t *au8PictureData)
		au8PictureData = contains datas for picture

	Decode:
	Get one byte, put it to the output file, and now it's the 'last' byte. 
	Loop 
	Get one byte 
	Is the current byte equal to last? 
	Yes 
		Now get another byte, this is 'counter' 
		Put current byte in the output file 
		Copy 'counter' times the 'last' byte to the output file 
		Put last copied byte in 'last' (or leave it alone) 
	No 
		Put current byte to the output file 
		Now 'last' is the current byte 
	Repeat. 
-------------------------------------------------------------------------------*/
void GLCD_DisplayPicture (const uint8_t *pointer) {
	unsigned char u8Page=0;
  	unsigned char u8Column=0;
	unsigned char data=0;
	unsigned char count=0;

	i2c_start(0b01111000);
  	LcdInstructionWrite(LCD_SET_PAGE);
    LcdInstructionWrite(LCD_SET_COL_HI);	// Set column at 0
    LcdInstructionWrite(LCD_SET_COL_LO);
	i2c_stop();

	LCD_start_Data_transfer();
  	for (u8Page = 0; u8Page < 8; u8Page++) { /* loop on the 8 pages */
		for (u8Column=0; u8Column<128; u8Column++) {
			if(count==0) {
				data = pgm_read_byte_near(pointer++);
				if(data==pgm_read_byte_near(pointer++)) {
					count = pgm_read_byte_near(pointer++);
				}
				else {
					count = 1;
					pointer--;
				}
			}
			count--;
			LcdDataWrite(data);
		}
	}
	i2c_stop();
}

/*-------------------------------------------------------------------------------
Send datas to the LCD
	LcdDataWrite (uint8_t u8Data)
		u8Data = data to send to the LCD
-------------------------------------------------------------------------------*/
void LcdDataWrite (uint8_t u8Data) {
	i2c_write(u8Data);
}

/*-------------------------------------------------------------------------------
Send instruction to the LCD
	LcdDataWrite (uint8_t u8Instruction)
		u8Instruction = Instruction to send to the LCD
-------------------------------------------------------------------------------*/
void LcdInstructionWrite (uint8_t u8Instruction) {
	i2c_write(0x80);
	i2c_write(u8Instruction);	
}

// LCDWaitBusy Not necessary, LCD controller is very fast
/*-------------------------------------------------------------------------------
Wait as long as the LCD is busy
	LcdWaitBusy();
-------------------------------------------------------------------------------*/
/*void inline LcdWaitBusy () {
	uint8_t time_out = 255;
    uint8_t status = BUSY;

	setbit(LCD_CTRL,LCD_RW);    // Read mode
	LCD_DDR = 0x00;		        // LCD_DATA input   // 31.25nS
    clrbit(LCD_CTRL,LCD_RS);    // Instruction mode // 31.25nS

    // Ignore first read
    setbit(LCD_CTRL,LCD_E);     // Strobe
    // 60nS minimum strobe pulse
    asm("nop"); // 31.25 nS
    asm("nop");
    clrbit(LCD_CTRL,LCD_E);    // Instruction mode // 31.25nS
    asm("lpm"); // 93.75 nS
    asm("lpm"); // 93.75 nS

	while ((status & BUSY) && (time_out--)) {	// Test the BUSY bit
        setbit(LCD_CTRL,LCD_E);     // Strobe
        // 60nS minimum strobe pulse
        asm("nop"); // 31.25 nS
        asm("nop");
        status = LCD_DATA_IN;
        clrbit(LCD_CTRL,LCD_E);    // Instruction mode // 31.25nS
        asm("lpm"); // 93.75 nS
        asm("lpm"); // 93.75 nS
    }
    LCD_DDR = 0xFF;				// set LCD_DATA port in output mode
}*/

/*-------------------------------------------------------------------------------
Clear the LCD screen
	GLCD_ClearScren ()
-------------------------------------------------------------------------------*/
void lcd_clear_graphics(void) {
	unsigned char u8Page,u8Column;
	unsigned char fill = LCD_Negative ? 0xff : 0;

	i2c_start(0b01111000);
	LcdInstructionWrite(LCD_SET_PAGE);
	LcdInstructionWrite(LCD_SET_COL_HI);	// Set column at 0
	LcdInstructionWrite(LCD_SET_COL_LO);
	i2c_stop();

	/* process the 8 pages of the LCD */
	LCD_start_Data_transfer();
	for(u8Page=0; u8Page<8; u8Page++) {
		for(u8Column=0; u8Column<128; u8Column++) {
			LcdDataWrite(fill);
		}
	}
	i2c_stop();
}

/*-------------------------------------------------------------------------------
Read data from LCD
	uint8_t = LcdDataRead();
-------------------------------------------------------------------------------*/
uint8_t LcdDataRead(void) {
/* TODO
	uint8_t data;
 	//LcdWaitBusy ();				// wait until LCD not busy
	setbit(LCD_CTRL,LCD_RW);		    // Read mode
    // 140nS Address setup time before E Strobe
	LCD_DDR = 0x00;		        // LCD_DATA input   // 31.25nS
    setbit(LCD_CTRL, LCD_RS);   // Data mode        // 31.25nS

    // 300nS minimum cycle time
    setbit(LCD_CTRL, LCD_E);		    // Strobe up
    // 60nS minimum strobe pulse
    asm("nop"); // 31.25 nS
    asm("nop");
    asm("lpm"); // 93.75 nS
    clrbit(LCD_CTRL, LCD_E);		    // Strobe down
    asm("lpm"); // 93.75 nS
    asm("lpm"); // 93.75 nS
    asm("lpm"); // 93.75 nS
    setbit(LCD_CTRL, LCD_E);		    // Strobe up
    // 60nS minimum strobe pulse
    asm("nop"); // 31.25 nS
    asm("nop");
    asm("lpm"); // 93.75 nS   asm("lpm"); // 93.75 nS
	data = LCD_DATA_IN;
    clrbit(LCD_CTRL,LCD_E);             // Strobe down

	LCD_DDR = 0xFF;		// set LCD_DATA port in output mode
	return data;		// return the data read*/
    return 0;
}

void LcdPageSet(uint8_t page) {
	LcdInstructionWrite(LCD_SET_PAGE + (page & 0x07));
}

void LcdXset(uint8_t x) { 
    LcdInstructionWrite(LCD_SET_COL_HI | (x >> 4));	// Set column at 2
    LcdInstructionWrite(LCD_SET_COL_LO | (x & 0x0F));
}

void lcd_gotoxy(uint8_t x, uint8_t y) {
	i2c_start(0b01111000);
    LcdXset(x);
    LcdPageSet(y>>3);
	i2c_stop();
}

void lcd_goto_x_page(uint8_t x, uint8_t page)
{
	i2c_start(0b01111000);
	LcdXset(x);
	LcdPageSet(page);
	i2c_stop();
}

// Clear display buffer
void clr_display(void) {
    lcd_clear_graphics();
}

// Transfer display buffer to LCD
void show_display(void) {
}

// Set pixel on display buffer
void set_pixel(uint8_t x, uint8_t y) {
    uint8_t data;
    lcd_gotoxy(x, y);
    data = LcdDataRead();
    data |= 0x01 << (y&0x07);				// set dot
    lcd_gotoxy(x, y);

    LCD_start_Data_transfer();
    LcdDataWrite(data);
    i2c_stop();
}

// Write byte on display buffer
void write_display(uint8_t data) {
	lcd_goto_x_page(dispx++, dispPage);
	
    LCD_start_Data_transfer();
    LcdDataWrite(data);
    i2c_stop();
}

// Write 0 on display buffer
void zero_display() {
	i2c_start(0b01111000);
    LcdXset(dispx++);
    LcdPageSet(dispPage);
	i2c_stop();
	
    LCD_start_Data_transfer();
    LcdDataWrite(0);
    i2c_stop();
}

//-----------------------------------------------------------------------
void lcd_line(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2) {
    uint8_t i,dxabs,dyabs,x,y;
    int8_t dx,dy,stepx,stepy;
    dx=(int8_t)x2-x1;      // the horizontal distance of the line
    dy=(int8_t)y2-y1;      // the vertical distance of the line
    if(dy<0) { dyabs=-dy; stepy=-1; }
    else { dyabs=dy; stepy=1; }
    if(dx<0) { dxabs=-dx; stepx=-1; }
    else {dxabs=dx; stepx=1; }
    x=(uint8_t)(dyabs>>1);
    y=(uint8_t)(dxabs>>1);
    set_pixel(x1,y1);
    if (dxabs>=dyabs) { // the line is more horizontal than vertical
        for(i=0;i<dxabs;i++) {
            y+=dyabs;
            if (y>=dxabs) {
                y-=dxabs;
                y1+=stepy;
            }
            x1+=stepx;
            set_pixel(x1,y1);
        }
    }
    else {  // the line is more vertical than horizontal
        for(i=0;i<dyabs;i++) {
            x+=dxabs;
            if (x>=dyabs) {
                x-=dyabs;
                x1+=stepx;
            }
            y1+=stepy;
            set_pixel(x1,y1);
        }
    }
}

/*-------------------------------------------------------------------------------
Print a char on the LCD
	GLCD_Putchar (uint8_t u8Char)
		u8Char = char to display
-------------------------------------------------------------------------------*/

void GLCD_Putchar(char u8Char) {
	// dispx - ��, � ������ ����� ������ ����� �� X
	// dispPage - �� ����� �������� �� ��������� ����������
	
	void newline()
	{
		dispx = 0;
		++dispPage;
		if (dispPage >= LCD_PAGES_COUNT)
			dispPage = 0;  // ��������� ������ �� ������ ������
	}
	
	// ������� ��������� �������
	// ������� �������� �����������, ����� �������� ������ ������������ �������
	/************************************************************************/
	/* ������ ������� 1 �������:											*/
	/* [<���� 1><����2>..<���� font->u8Width - 1>]							*/
	/*																		*/
	/* ������ ������� 2 ��������:											*/
	/* [<���� 1><����2>..<���� font->u8Width - 1>]							*/
	/* [<���� font->u8Width>..<���� (font->u8Width)*2 - 1>]					*/
	/*                                                                      */
	/************************************************************************/
	
	// �������� ������� �� ����� ������
	if (u8Char == '\n') 
	{	// New line
		newline();
		return;
	}
		
	if (u8Char == '\t')
	{
		lcd_putsp(PSTR("    ")); //4 ������� ������ ����
		return;
	}
	
	LoadLeter(u8Char);  // ���������� ����� � CharactecterBufer
	const uint8_t charWidth = font->u8Width;
		
	const uint8_t charHeight_p = (font->u8Height - 1) / 8 + 1; // ������ ������� � ���������
	
	/*
	if (font == &Font_Digits_big)
		u8Char = charHeight_p + 0x30;
	*/
		
	// ������� ������ �� ������?
	if (dispx + charWidth + 1 > LCD_PAGE_COLUMS)
		newline();
		
	// ����������� �������� ��� "��������"
	uint8_t charColumns = charWidth + char2charInterval;
	if (font == &FONT_SPECIAL_SYMBOLS)
		charColumns = charWidth;
		
	// ����� �������
	for(uint8_t page = 0; page < charHeight_p; ++page) //��������
	{
		lcd_goto_x_page(dispx, dispPage + page); // ������� �� ������ �������
		LCD_start_Data_transfer();
		for (uint8_t column = 0; column < charColumns; ++column) // �������
		{
			char data = 0;
			if (column < charWidth) // ����������� �������?
				data = CharactecterBufer[page * charWidth + column]; //������ ������ � �������
			if (LCD_Negative)
				data = ~data; //�������
			LcdDataWrite(data); // display increment column address itself
		}
		i2c_stop();
	}
	dispx += charColumns;
}

/*-------------------------------------------------------------------------------
Print a string on the LCD
	GLCD_Printf (uint8_t *au8Text) 
		*au8Text = string to display
-------------------------------------------------------------------------------*/
uint8_t lcd_putsp (const char *ptr) {
    uint8_t n=0;
    while (pgm_read_byte(ptr) != 0x00) {
        GLCD_Putchar(pgm_read_byte(ptr++));
        n++;
    }
    return n;
}

void lcd_puts (const char *ptr) {
	while(*ptr) GLCD_Putchar (*ptr++);
}

// Print Number
void printN(int16_t Data) {
	/*
    uint8_t D1=0,D2=0,D3=0,D4=0,D5=0;
    if(Data<0) {
        Data=-Data;
        GLCD_Putchar('-');
    }
    else GLCD_Putchar(' ');
	while (Data>=10000)	{ D5++; Data-=10000; }
	while (Data>=1000)	{ D4++; Data-=1000; }
	while (Data>=100)	{ D3++; Data-=100; }
	while (Data>=10)	{ D2++; Data-=10; }
	while (Data>=1)		{ D1++; Data-=1; }
    GLCD_Putchar(0x30+D5);
    GLCD_Putchar(0x30+D4);
    GLCD_Putchar(0x30+D3);
    GLCD_Putchar(0x30+D2);
    GLCD_Putchar(0x30+D1);
    GLCD_Putchar(' ');
	*/
	uint8_t D[5] = {0, 0, 0, 0, 0};
	uint8_t force = 0;
	if(Data<0) {
        Data=-Data;
        GLCD_Putchar('-');
    }
	while (Data>=10000)	{ D[4]++; Data-=10000; }
	while (Data>=1000)	{ D[3]++; Data-=1000; }
	while (Data>=100)	{ D[2]++; Data-=100; }
	while (Data>=10)	{ D[1]++; Data-=10; }
	while (Data>=1)		{ D[0]++; Data-=1; }
	for (signed char i = 4; i >=0 ; --i)
		if (D[i] || force)
			{
				force = 1;
				GLCD_Putchar(0x30+D[i]);
			}
}

// Print Fixed point Number
// 5 decimal digits
/*
void printF(uint8_t x, uint8_t y, int32_t Data) {
	uint8_t D[4]={0,0,0,0},point=0,i,j=1;
 	uint16_t n;
    uint8_t Stat;
    Stat = SREG;        // Save global interrupt bit
    //cli();              // Disable interrupts
    lcd_goto(x*8,y/8);
    if(font == &Font_8x12) {
 	    if(Data<0) { GLCD_Bigchar('-'); Data = -Data; }
        else GLCD_Bigchar(' ');
    }
    else {
 	    if(Data<0) { GLCD_Putchar('-'); Data = -Data; }
        else GLCD_Putchar(' ');
    }
	if(Data>=999900000L) n = 9999;
	else if(Data>=100000000L)  n = (uint16_t)(Data/100000);
	else if(Data>=10000000L) {
		n = (uint16_t)(Data/10000);
		point = 1;
	}
	else if(Data>=1000000L) {
		n = (uint16_t)(Data/1000);
		point = 2;
	}
	else {
		n = (uint16_t)(Data/100);
		point = 3;
	}

	while (n>=1000) { D[3]++; n-=1000; }
	while (n>=100)	{ D[2]++; n-=100; }
	while (n>=10)	{ D[1]++; n-=10; }
	while (n>=1)	{ D[0]++; n-=1; }

	for(i=3; i!=255; i--) {
		if(font == &Font_8x12) {
			GLCD_Bigchar(D[i]);
			if(point==i) GLCD_Bigchar('.');
		}
		else {
			GLCD_Putchar(0x30+D[i]);
			if(point==i) GLCD_Putchar('.');
		}
	}
    //if(testbit(Stat, CPU_I_bp)) 
		//sei();
}//*/

// Print small font text from program memory
uint8_t tiny_printp(uint8_t x, uint8_t y, const char *ptr) {
    lcd_goto(x*8,y/8);
	return lcd_putsp(ptr);;
	}
