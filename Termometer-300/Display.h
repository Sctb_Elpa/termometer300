/*
 * Display.h
 *
 * Created: 15.05.2014 13:03:09
 *  Author: tolyan
 */ 


#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <stdbool.h>

typedef struct  
{
	 float Temperature;
	 float F_temperature;
	 char* lastStr;
} sDisplay_vars;

// hard == 1 - перерисовать полностью
void RefrashDisplay();

void prepareScreen();
void drawTemperature(char* buf);

void display_on();
void display_off();

extern sDisplay_vars dataOutputVars;
extern uint8_t forceDisplayRefrash;

#endif /* DISPLAY_H_ */