/*
 * Settings.c
 *
 * Created: 29.05.2014 14:17:36
 *  Author: tolyan
 */ 

#include <avr/eeprom.h>

#include "Settings.h"

struct sSettings settings EEMEM =
{
	.TemperatureCoeffs = {
		.F0 = 0,
		.T0 = 0,
		.C = {1, 0, 0}
		},	
	.MesureTime = 1,
	.DisplyaFreq = 0
};