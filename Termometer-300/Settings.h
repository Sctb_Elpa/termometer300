/*
 * Settings.h
 *
 * Created: 29.05.2014 14:18:19
 *  Author: tolyan
 */ 


#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <avr/eeprom.h>

#include "Counters.h"

#define USB_ENABLED						1
#define DISPLAY_ENABLED					1
#define MESURMENT_ENABLE				1

struct sSettings
{
	// ������������ ��� ������� �����������
	struct sTemperatureCoeffs		TemperatureCoeffs;
	
	// ����� �����������
	float						MesureTime;
	
	// ���������� ������� ������ �����������
	uint8_t						DisplyaFreq;
};

extern struct sSettings settings;

#endif /* SETTINGS_H_ */