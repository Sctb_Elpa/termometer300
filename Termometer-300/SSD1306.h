/* ----------------------------------------------------------------------------
	128x64 Graphic LCD management for SSD1306 driver

	FILE NAME 	: SSD1306.h
	
	DESCRIPTION	: The purpose of this function is to manage a graphic LCD
			  by providing function for control and display text and graphic 
			  
	AUTHOR		: Gabriel Anzziani
    www.gabotronics.com

*/

#ifndef SSD1306_H
#define SSD1306_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define     LCD_RES         PC2           // RESET
#define		LCD_PORT		PORTC
#define		LCD_DDR		    DDRC

// commands SH1101A controller
#define LCD_SET_COL_HI		0x10
#define LCD_SET_COL_LO		0x00
#define LCD_SET_LINE		0x40
#define LCD_SET_CONTRAST	0x81
#define LCD_SET_SEG_REMAP0  0xA0
#define LCD_SET_SEG_REMAP1	0xA1
#define LCD_EON_OFF			0xA4
#define LCD_EON_ON			0xA5
#define LCD_DISP_NOR		0xA6
#define LCD_DISP_REV		0xA7
#define LCD_MULTIPLEX       0xA8
#define LCD_CHARGE_PUMP    	0x8D
#define LCD_PUMP_OFF    	0x10
#define LCD_PUMP_ON     	0x14
#define LCD_DISP_OFF 		0xAE
#define LCD_DISP_ON			0xAF
#define LCD_SET_PAGE		0xB0
#define LCD_SET_SCAN_FLIP	0xC0
#define LCD_SET_SCAN_NOR	0xC8
#define LCD_SET_OFFSET		0xD3
#define LCD_SET_RATIO_OSC	0xD5
#define LCD_SET_CHARGE  	0xD9
#define LCD_SET_PADS    	0xDA
#define LCD_SET_VCOM    	0xDB
#define LCD_NOP     		0xE3
#define LCD_SCROLL_RIGHT	0x26
#define LCD_SCROLL_LEFT		0x27
#define LCD_SCROLL_VR	    0x29
#define LCD_SCROLL_VL		0x2A
#define LCD_SCROLL_OFF		0x2E
#define LCD_SCROLL_ON   	0x2F
#define LCD_SCROLL_ON   	0x2F
#define LCD_VERT_SCROLL_A  	0xA3
#define LCD_MEM_ADDRESSING 	0x20
#define LCD_SET_COL_ADDR	0x21
#define LCD_SET_PAGE_ADDR	0x22

#define LCD_PAGES_COUNT		8
#define LCD_PAGE_COLUMS		128


extern      uint16_t    pointer;
extern      uint8_t     Underline;
extern      uint8_t     LCD_Negative;
extern		uint8_t		char2charInterval;
extern      uint8_t     Sub;

extern struct FONT_DEF *font;

#define lcd_goto(x,y) { dispx=(x); dispPage=(y); }
/* EXTERN Function Prototype(s) */
void LCD_start_Data_transfer(void);
uint8_t isDisplayOn();
void LcdDataWrite (unsigned char);
uint8_t GLCD_on(char on);
void GLCD_LcdInit (void);	
void lcd_clear_graphics (void);
void GLCD_DisplayPicture (const uint8_t *pointer);
//void GLCD_Locate (uint8_t, uint8_t);
void lcd_goto_x_page(uint8_t x, uint8_t page);
void GLCD_Print (const char *);
uint8_t lcd_putsp(const char *);
void lcd_puts (const char *);
void GLCD_Putchar (char);
void printN(int16_t Data);
void clr_display(void);
void show_display(void);
void set_pixel(uint8_t x, uint8_t y);
void lcd_line(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2);
void write_display(uint8_t data);
void zero_display();
//void printF(uint8_t x, uint8_t y, int32_t Data);
uint8_t tiny_printp(uint8_t x, uint8_t y, const char *ptr);
uint8_t LcdDataRead(void);
void lcd_gotoxy(uint8_t x, uint8_t y);

/******************************************************************************
/                       DECLARATIONS / DEFINITIONS                            /
/ ****************************************************************************/

/* Global variables */
extern unsigned char dispPage, dispx;

#ifdef __cplusplus
}
#endif

#endif
