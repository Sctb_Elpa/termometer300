/*
 * Counters.h
 *
 * Created: 16.05.2014 15:29:18
 *  Author: tolyan
 */ 


#ifndef COUNTERS_H_
#define COUNTERS_H_

struct sTemperatureCoeffs
{
	float T0;
	float F0;
	float C[3];
};

struct sPressureCoeffs
{
	float Ft0;
	float Fp0;
	float A[6];
};

void EnableCounters(uint8_t enable);
void calcFsensOutputVals(float* TemperatureResult, float* fTemperature);

#endif /* COUNTERS_H_ */