﻿/*
 * GccUSB.cpp
 *
 * Created: 29.04.2014 8:11:08
 *  Author: max
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <stdio.h>

#include "SoftI2CMaster.h"
#include "SSD1306.h"
#include "GenericHID.h"
#include "Fonts.h"
#include "Display.h"
#include "Counters.h"
#include "Settings.h"
	
void init_all()
{
	i2c_init(); // I2C
	i2c_SetSpeed(I2C_SPEED_FAST);
	
#if MESURMENT_ENABLE == 1	
	EnableCounters(1);
#endif
	
#if DISPLAY_ENABLED == 1
	GLCD_LcdInit(); // монитор
	prepareScreen();
	GLCD_on(1); // включить монитор
#endif
	
#if USB_ENABLED == 1
	USB_Init();
#endif		
		
	GlobalInterruptEnable(); // sei()
}

int main(void)
{	  
	init_all();
		
	while(1)
    {	
		if (forceDisplayRefrash)
		{
			forceDisplayRefrash = 0;
			
#if MESURMENT_ENABLE == 1
			calcFsensOutputVals(&dataOutputVars.Temperature, &dataOutputVars.F_temperature); // температура, давление
#endif

#if DISPLAY_ENABLED == 1
			RefrashDisplay();
#endif
		}
#if USB_ENABLED == 1		
		// process USB events
		LUFA_main();
#endif	
    }
}

// default interrupt routine
ISR(BADISR_vect)
{
}
