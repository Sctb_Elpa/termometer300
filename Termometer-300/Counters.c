/*
 * Counters.c
 *
 * Created: 16.05.2014 15:28:33
 *  Author: tolyan
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>

#include "Settings.h"
#include "Display.h"

#include "Counters.h"

#define START_FREQ				(1000.0) // ��������� ������� (���� ������ �� �����)

/************************************************************************/
/* T0 8bit - ������� ������� ��������									*/
/* T1 16bit - ������� ������� �������									*/                                                                     
/************************************************************************/

union uReferenceFreqCounter
{
	struct 
	{
		uint16_t REGISTER;
		uint16_t Extantion;
	} s;	
	uint32_t value;
};

// ���������� �������� ������� �������
uint16_t RefFreqTimer_Ovf;

union uTemperatureFreqCounter
{
	struct  
	{
		uint8_t REGISTER;
		uint16_t Extantion;
	} s;
	uint32_t value;
};

static union uTemperatureFreqCounter	TemperatureFreqCounter_Work,	// ����� � ������
										TemperatureFreqCounter_Target,  // ��� ���������� ����������
										TemperatureFreqCounter_Target_was,
										TemperatureFreqCounter_Target_new;
									
static union uReferenceFreqCounter		TemperatureMesureValue_Start,  // �������� �������� ������� ������� � ������ �����
										TemperatureMesureValue_Result;  // ���������

float Ft = START_FREQ;											// ������� �����������

// ��������� �����, �� �������� ��������� �������, ���� �� ���� �������� �������
// AproxFreq, �� ���� ��������� STANDART_MESURE_TIME
static uint32_t calcTargetvalue(float F)
{
	float mesureTime;
	eeprom_read_block(&mesureTime, &settings.MesureTime, sizeof(float));
	uint32_t res = (uint32_t)(/*F*/550 * mesureTime);
	return res ? res : 1;
}

// ��������� �������� ����������� � �������� �� ��������
void calcFsensOutputVals(float* TemperatureResult, float* fTemperature)
{
	/*�������� ����������*/
	struct sTemperatureCoeffs TemperatureCoeffs;
	eeprom_read_block(&TemperatureCoeffs, &settings.TemperatureCoeffs, sizeof(struct sTemperatureCoeffs));
	/********************/
	
	// ������� �����������
	// TemperatureMesureValue_Result - ���������� ��������� ������� F_CPU �� �����, ������� ������������ �� ������ TemperatureFreqCounter_Target ���������
	// ���������� �������
	Ft =  F_CPU * (float)TemperatureFreqCounter_Target_was.value / (float)TemperatureMesureValue_Result.value;
	float TempF_minus_Ft0 = Ft - TemperatureCoeffs.F0;
	float TempF_minus_Ft0_w = TempF_minus_Ft0;
	
	float Temperature_fSens = TemperatureCoeffs.T0;
	for (unsigned char i = 0; i < 3; ++i, TempF_minus_Ft0_w *= TempF_minus_Ft0)
		Temperature_fSens += TemperatureCoeffs.C[i] * TempF_minus_Ft0_w;
		
	*TemperatureResult = Temperature_fSens;
	
	if (fTemperature)
		*fTemperature = Ft;
		
	TemperatureFreqCounter_Target_new.value = calcTargetvalue(Ft);
}

void EnableCounters(uint8_t enable)
{
	if (enable)
	{
		// ����� ��� ��������?
		if (TCCR0B == ((1 << CS02) | (1 << CS01) | (1 << CS00)))
			return;
		
		// ����� �� power_down
		PRR0 &= ~((1 << PRTIM0) | (1 << PRTIM1));
		
		// ��������� ��������
		TemperatureMesureValue_Start.value = 0;
		TemperatureFreqCounter_Target_new.value = TemperatureFreqCounter_Target_was.value = TemperatureFreqCounter_Target.value = calcTargetvalue(Ft);
		
		// �������� ������� ������� �������
		TCCR1A = 0;			 // no output
		TCCR1B = 1 << CS10;	 // Fin = F_CPU
		TIMSK1 = 1 << TOIE1; // ��������� ����������
		
		// �������� ������� ������� ��������
		DDRD &= ~(1 << 7);
		TCCR0A = 0;			// no output
		TCCR0B = (1 << CS02) | (1 << CS01) | (1 << CS00); // rising age
		TIMSK0 = 1 << TOIE0; // ��������� ����������
		
		// � ����� ������� ��� ����������� ������ � ����������
	}
	else
	{
		// ��������� ����� ���������
		
		TCCR1B = 0;
		TCCR0B = 0;
		
		// powwer down
		PRR0 |= (1 << PRTIM0) | (1 << PRTIM1);
	}
}

ISR(TIMER0_OVF_vect)
{
	// ������ �������� �������� ������� �������
	union uReferenceFreqCounter curentVal;
	curentVal.s.REGISTER = TCNT1;
	curentVal.s.Extantion = RefFreqTimer_Ovf;
	
	// ������� ������ ������ �����
	if (TemperatureMesureValue_Start.value == 0)
	{
		// ����������� ����� ������
		TemperatureMesureValue_Start.value = curentVal.value;
		
		TemperatureFreqCounter_Work.value = TemperatureFreqCounter_Target.value; // ������������ ��������
	}
	if (TemperatureFreqCounter_Work.s.Extantion) 
		--TemperatureFreqCounter_Work.s.Extantion; // �� 0
	else
	{
		if (TemperatureFreqCounter_Work.s.REGISTER)
		{
			if (TCNT0 > TemperatureFreqCounter_Work.s.REGISTER)
			{
				curentVal.s.REGISTER = TCNT1;
				curentVal.s.Extantion = RefFreqTimer_Ovf;
				goto __end_cycleT0;
			}
			// ��������� �������
			TCNT0 -= TemperatureFreqCounter_Work.s.REGISTER;
			TemperatureFreqCounter_Work.s.REGISTER = 0;
		}
		else
		{
__end_cycleT0:
			// ��� ���������
			TemperatureMesureValue_Result.value = curentVal.value - TemperatureMesureValue_Start.value;
			TemperatureMesureValue_Start.value = 0;
			TemperatureFreqCounter_Target_was.value = TemperatureFreqCounter_Target.value;
			TemperatureFreqCounter_Target.value = TemperatureFreqCounter_Target_new.value; // update mesure time
			
			forceDisplayRefrash = 1;

			TCNT0 = 0xff; // ��������� ������� ���������� ������� �������� ����� ����
		}
	}
}

ISR(TIMER1_OVF_vect)
{
	++RefFreqTimer_Ovf;
}
