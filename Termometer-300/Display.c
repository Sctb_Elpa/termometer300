/*
 * Display.c
 *
 * Created: 15.05.2014 13:00:33
 *  Author: tolyan
 */ 

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

#include "SSD1306.h"

#include "Fonts.h"
#include "SoftI2CMaster.h"
#include "SSD1306.h"
#include "Settings.h"

#include "Display.h"

#define TEMPERATURE_POSITION_Y		3

static char message[20] = "";

uint8_t forceDisplayRefrash;

uint8_t prevStrLen = 0;

sDisplay_vars dataOutputVars = 
{
	.Temperature = 0.0,
	.F_temperature = 0.0,
	.lastStr = message
};  // �� ���� ������ �������� ��������

static const char tempertureCaption[] PROGMEM = "TEMPERATURE (\370C)";

static char* _printfn(char *buf, uint8_t N)
{
	for (uint8_t i = 0; i < N; ++i, ++buf)
		GLCD_Putchar(*buf);
	return buf;
}

// ������ ������ ����� � �������� �������� ������� ��������
static void PreparePopupBackground(uint8_t popup_width, uint8_t popup_height)
{
	// ����� �� �����
	popup_width += 4; //2 �������, 2 '||'
	popup_height += 2; // 2 x ====
	
	popup_width += popup_width / 9;
	
	void printHorisontalBorder()
	{
		for(uint8_t c = 1; c < popup_width - 1; ++c)
			GLCD_Putchar(SYMBOL_HORISONTAL_DOUBLE_LINES);
	}
	
	void printHorisontalLine()
	{
		GLCD_Putchar(SYMBOL_VERTICAL_DOUBLE_LINES_CONER);
		for(uint8_t c = 1; c < popup_width - 1; ++c)
			GLCD_Putchar(' ');
		//dispx += (popup_width - 2) * Font_5x7.u8Width;
		GLCD_Putchar(SYMBOL_VERTICAL_DOUBLE_LINES_CONER);
	}
	
	void resetXPos()
	{
		dispx = LCD_PAGE_COLUMS / 2 - (popup_width * Font_5x7.u8Width) / 2 ;
	}
	
	font = &Font_5x7;
	uint8_t oldinterval = char2charInterval; // save interval
	char2charInterval = 0;
	
	dispPage = LCD_PAGES_COUNT / 2 - popup_height / 2 /*+ 1*/;
	resetXPos();
	GLCD_Putchar(SYMBOL_LEFT_UP_DOUBLE_LINES_CONER);
	printHorisontalBorder();
	GLCD_Putchar(SYMBOL_RIGHT_UP_DOUBLE_LINES_CONER);
	
	++dispPage;
	resetXPos();
	for (uint8_t page = 1; page < popup_height - 1; ++page, ++dispPage)
	{
		resetXPos();
		printHorisontalLine();
	}
	
	resetXPos();
	GLCD_Putchar(SYMBOL_LEFT_DOWN_DOUBLE_LINES_CONER);
	printHorisontalBorder();
	GLCD_Putchar(SYMBOL_RIGHT_DOWN_DOUBLE_LINES_CONER);

	char2charInterval = oldinterval; //restore interval;
}

static void drawPopupText(uint8_t column, uint8_t page, uint8_t* text)
{
	lcd_goto(column, page);
	while (1)
	{
		uint8_t c = *text++;
		if (c == '\0')
			return;
		if (c == '[')
		{
			LCD_Negative = 1;
			continue;
		}
		if (c == ']')
		{
			LCD_Negative = 0;
			continue;
		}
		if (c == '\n')
		{
			dispx = column;
			++dispPage;
			continue;
		}
		GLCD_Putchar(c);
	}
}

static void Display_on()
{
	if (GLCD_on(1))
		prepareScreen();
}

void RefrashDisplay()
{
	char buff[30];
	
	// [DEBUG]
	font = &Font_5x7;
	lcd_goto(0, 7);
	lcd_puts(dataOutputVars.lastStr);
	// [/DEBUG]
	
	uint8_t mode = eeprom_read_byte(&settings.DisplyaFreq);
	
__draw_normal_:	

	//sprintf(buff, "%+6.2f", displayVars.Temperature); // C +
	if (mode)
		sprintf(buff, "%7.3f", dataOutputVars.F_temperature); // ������
	else
		sprintf(buff, "%+4.2f", dataOutputVars.Temperature); // �����������
	drawTemperature(buff);
}
 
void prepareScreen()
{
	for (uint8_t Quater_page = 0; Quater_page < LCD_PAGES_COUNT * 4; ++Quater_page)
	{
		lcd_goto_x_page((Quater_page % 4) * (LCD_PAGE_COLUMS / 4), Quater_page >> 2);
		LCD_start_Data_transfer();
		for (uint16_t column = 0; column < LCD_PAGE_COLUMS / 4; ++column)
		{
			uint8_t data = 0;
			if (LCD_Negative)
				data = ~data;
			LcdDataWrite(data);
		}
		i2c_stop();
	}
	PreparePopupBackground(strlen_P(tempertureCaption), (Font_Digits_big.u8Height - 1) / 8 + 1/* + 1*/);
	/*
	font = &Font_5x7;
	lcd_goto(LCD_PAGE_COLUMS / 2 - (strlen_P(tempertureCaption) * (Font_5x7.u8Width + 1)) / 2, TEMPERATURE_POSITION_Y);
	lcd_putsp(tempertureCaption);
	*/
}

void drawTemperature(char* buf)
{
	uint8_t leterscount = strlen(buf);
	if (leterscount != prevStrLen)
	{
		prevStrLen = leterscount;
		prepareScreen();
	}
	uint8_t width = leterscount * (Font_Digits_big.u8Width + 1);
	//PreparePopupBackground(leterscount * Font_Digits_big.u8Width / Font_5x7.u8Width, (Font_Digits_big.u8Height - 1) / 8 + 1 + 1);
	
	font = &Font_Digits_big;
	lcd_goto(LCD_PAGE_COLUMS / 2 - (width) / 2, TEMPERATURE_POSITION_Y /*+ 1*/);
	lcd_puts(buf);
}

