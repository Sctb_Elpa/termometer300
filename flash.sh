#!/bin/bash

DIR=`cat *.atsln  | grep Project | cut -d ',' -f2 | grep cproj | cut -d '\' -f 1 | sed 's/\"//' | sed 's/ //'`

BATCHIP="/c/Program Files/Atmel/Flip 3.4.7/bin/batchisp.exe"
#BATCHIP="c:\Program Files\Atmel\Flip 3.4.7\bin\batchisp.exe"
HEX=$DIR/Debug/$DIR.hex
EPP=$DIR/Debug/$DIR.eep
EPP_NEW=./$DIR.hex

cp $EPP $EPP_NEW

echo +++++EREASE+++++
"$BATCHIP" -hardware usb -device at90usb162 -operation onfail abort erase f

echo +++++FLASH+++++
"$BATCHIP" -hardware usb -device at90usb162 -operation onfail abort memory flash loadbuffer $HEX program VERIFY

echo +++++EEPROM+++++
"$BATCHIP" -hardware usb -device at90usb162 -operation onfail abort memory eeprom loadbuffer $EPP_NEW program VERIFY

echo +++++START NORESET+++++
"$BATCHIP" -hardware usb -device at90usb162 -operation onfail abort start noreset 0

rm $EPP_NEW